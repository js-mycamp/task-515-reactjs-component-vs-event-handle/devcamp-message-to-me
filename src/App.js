import imageLike from './assets/images/facebook-like-icon_1017-8081.avif'
import imageFb from './assets/images/Image.jpg'
import 'bootstrap/dist/css/bootstrap.min.css'
function App() {
  return (
    <div className='container text-center text-white bg-secondary' >
      <div className='row mt-5'>
        <h2> Chào mừng đến với Devcamp120</h2>;
      </div>

      <div className='row mt-2'>
        <div className='col-12'>
          <img src={imageFb} alt='chào mừng bạn đến với devcamp' style={{ width: "500px" }} />;
        </div>
      </div>

      <div className='col-12 mt-2'>
        <label className='form-floating'>Message cho bạn trong 12 tháng tới</label>
      </div>

      <div className='col-12 mt-2'>
        <input className='form-control' style={{ width: "500px", margin: "0 auto" }}></input>
      </div>
    
      <div className='col-12 mt-2'>
        <p > Message ở đây!</p>
      </div>

      <div className='row mt-2'>
        <div className='col-12'>
          <img src={imageLike} alt='like' style={{ width: "100px" }} />;
        </div>
      </div>
    
    </div>
  );
}

export default App;
